const assert = require(`assert`);
const script = require(`./../script`);


describe(`test a.js`, () => {
    assert.strictEqual(script.f(), 5);
    assert.strictEqual(script.g(), 10);
});
